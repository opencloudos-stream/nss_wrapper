Summary:        A wrapper for the user, group and hosts NSS API
Name:           nss_wrapper
Version:        1.1.13
Release:        5%{?dist}
License:        BSD
Url:            https://cwrap.org/
Source0:        https://ftp.samba.org/pub/cwrap/%{name}-%{version}.tar.gz
Patch3000:      nss_wrapper-fix-cmocka-1.1.6+-support.patch

BuildRequires:  cmake gcc gnupg2 libcmocka-devel perl-generators
Requires:       %{name}-libs = %version-%release
Recommends:     cmake pkgconfig

%description
There are projects which provide daemons needing to be able to create, modify
and delete Unix users. Or just switch user ids to interact with the system e.g.
a user space file server. To be able to test that you need the privilege to
modify the passwd and groups file. With nss_wrapper it is possible to define
your own passwd and groups file which will be used by software to act correctly
while under test.

If you have a client and server under test they normally use functions to
resolve network names to addresses (dns) or vice versa. The nss_wrappers allow
you to create a hosts file to setup name resolution for the addresses you use
with socket_wrapper.

To use it set the following environment variables:

LD_PRELOAD=libuid_wrapper.so
NSS_WRAPPER_PASSWD=/path/to/passwd
NSS_WRAPPER_GROUP=/path/to/group
NSS_WRAPPER_HOSTS=/path/to/host

This package doesn't have a devel package cause this project is for
development/testing.

%package libs
Summary: nss_library shared library only

%description libs
The %{name}-libs package provides only the shared library.
For a minimal footprint, install just this package.

%prep
%autosetup -p1

%build
%cmake \
  -DUNIT_TESTING=ON

%cmake_build

%install
%cmake_install

sed -i '1 s|/usr/bin/env\ perl|/usr/bin/perl|' %{buildroot}%{_bindir}/nss_wrapper.pl

%check
%ctest

%files
%{_bindir}/nss_wrapper.pl
%dir %{_libdir}/cmake/nss_wrapper
%{_libdir}/cmake/nss_wrapper/nss_wrapper-config-version.cmake
%{_libdir}/cmake/nss_wrapper/nss_wrapper-config.cmake
%{_libdir}/pkgconfig/nss_wrapper.pc
%{_mandir}/man1/nss_wrapper.1*

%files libs
%license LICENSE
%doc AUTHORS README.md CHANGELOG
%{_libdir}/libnss_wrapper.so*

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.13-5
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.13-4
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.13-3
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Aug 7 2023 Wang Guodong <gordonwwang@tencent.com> - 1.1.13-2
- Fix building with cmocka >= 1.1.6

* Thu Jul 13 2023 Wang Guodong <gordonwwang@tencent.com> - 1.1.13-1
- Upgrade to 1.1.13

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.11-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.11-2
- Rebuilt for OpenCloudOS Stream 23

* Tue Jan 31 2023 Xin Cheng <denisecheng@tencent.com> - 1.1.11-1
- Init package